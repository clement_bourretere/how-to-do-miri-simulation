#!/usr/bin/env python
#  -*- coding: utf-8 -*-

# MIRISim
from mirisim import MiriSimulation
from mirisim.skysim import *
from mirisim.config_parser import *  # SimConfig, SimulatorConfig, SceneConfig
import shutil

# We define the Simulation parameters for MIRISIM
simulator_config = SimulatorConfig.makeSimulator(
    take_webbPsf=False,
    include_refpix=True,
    include_poisson=False,
    include_readnoise=True,
    include_badpix=True,
    include_dark=True,
    include_flat=True,
    include_gain=True,
    include_nonlinearity=True,
    include_drifts=True,
    include_latency=False,
    cosmic_ray_mode='NONE')

# We prepare the scene we want to model
background = Background(level='low', gradient=0., pa=0.0, centreFOV=(0., 0.))

scene_config = SceneConfig.makeScene(loglevel=1, background=background, targets=[])

sim_config = SimConfig.makeSim(name="Default Simulation", rel_obsdate=0.0, scene="scene.ini",
                               POP='IMA', ConfigPath='LRS_SLITLESS', Dither=False, StartInd=1, NDither=2,
                               DitherPat="lrs_recommended_dither.dat",
                               filter="P750L", readDetect='SLITLESSPRISM', ima_mode='FAST', ima_exposures=1,
                               ima_integrations=1, ima_frames=50,  # 65,
                               disperser='SHORT', detector='SW', mrs_mode='FAST', mrs_exposures=1,
                               mrs_integrations=1, mrs_frames=1)

# We run the simulation
# loglevel : "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"
ms = MiriSimulation(sim_config=sim_config, scene_config=scene_config,
                    simulator_config=simulator_config, loglevel='WARNING')

ms.run()

# Delete the simulation we just created, because we're only interested in downloading the corresponding CDPs in
# preparation of the parallel run that will force local use.
shutil.rmtree(ms.path_out)
