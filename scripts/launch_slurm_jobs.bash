#!/bin/bash 
export PARENT=/beegfs/data/ah258874/exoplanets_simulation/HAT-P-14b-0-T0-2021-09-21-no-tso
export SCRIPTS_DIR=$PARENT/scripts
export SED_DIR=$PARENT/exonoodle_output
export LOG_DIR=$PARENT/log
export CALCULATION_DIR=$PARENT/mirisim_output

for folder in $(ls $SED_DIR)
do
    cd $SED_DIR/$folder
    sbatch $SCRIPTS_DIR/run_mirisim_pool.sh $folder $SCRIPTS_DIR $CALCULATION_DIR $LOG_DIR $SED_DIR
done

