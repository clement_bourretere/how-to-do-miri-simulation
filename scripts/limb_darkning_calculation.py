from exotethys import sail
import pickle
import numpy as np
import matplotlib.pyplot as plt
import re 
import os

os.system("ls")
sail_file=input("Sail file : ")
sail.ldc_calculate(sail_file)
os.system("ls")
pickle_file=input("Pickle file : ")
# SAIL FILE
sail_file=open(sail_file,'r')
k=0
for line in sail_file :
    if k==4 : stellar_model_grid=line
    if k==5 : limb_darkening_laws=line
    if k==9 : passbands=line
    if k==17 : target_name=line
    k+=1

    ## stellar_model_grid
str=stellar_model_grid
str_array=re.split(' |\t|\n',str)
str_array.pop(0)
stellar_model_grid_array=[]
for word in str_array :
    if not len(word)==0:
        stellar_model_grid_array.append(word)
for word in stellar_model_grid_array :
    if word[0]!='!': stellar_model_grid=word


    ## limb_darkening_laws
str=limb_darkening_laws
str_array=re.split(' |\t|\n',str)
str_array.pop(0)
limb_darkening_laws_array=[]
for word in str_array :
    if not len(word)==0:
        limb_darkening_laws_array.append(word)
for word in limb_darkening_laws_array :
    if word[0]!='!': limb_darkening_laws=word

    ## target_name 
str=target_name
str_array=re.split(' |\t|\n',str)
str_array.pop(0)
target_name_array=[]
for word in str_array :
    if not len(word)==0:
        target_name_array.append(word)
target_name=target_name_array[0]

   ## passbands
str=passbands
str_array=re.split(' |\t|\n',str)
str_array.pop(0)
passbands_array=[]
for word in str_array :
    if not len(word)==0:
        passbands_array.append(word)
passbands=passbands_array[0]

# READ COEFS
planet=pickle.load(open(pickle_file, 'rb'), encoding='latin1') # argv[1]=pickle file
star = planet['star_params']
print(star)

coefs = planet['passbands'][passbands][limb_darkening_laws]['coefficients']
#print("coefs="+str(coefs))
# GET LD INTENSITIES
mu = np.arange(101)/100 #from the outer part 0 to the center of the star 1
inte = sail.get_intensities_from_ldcs(mu, coefs, limb_darkening_laws)

# PLOT LD
plt.figure(target_name+'_intensity_profile_for_'+limb_darkening_laws+'_limb-darkening_coeff', figsize=(8,6))
#plt.figure('HATP-12b_intensity_profile_for_quadratic_limb-darkening_coeff', figsize=(8,6))
plt.plot(mu, inte, label=stellar_model_grid+'-'+limb_darkening_laws)

#plt.title('HATP-12b intensity profile for quadratic limb-darkening coefficients')
plt.title(target_name+' intensity profile for '+limb_darkening_laws+' limb-darkening coefficients')
a,b=coefs[0],coefs[1]
a = "{:.8f}".format(a)
b = "{:.8f}".format(b)
plt.xlabel('Stellar radius mu coefficient - 1: center, 0: outer edge\ncoeffs = ['+a+" ,"+b+" ]")
plt.ylabel('Star flux (ergs/cm2/A/s/sr)')
plt.legend()
plt.savefig(target_name+' intensity profile for '+limb_darkening_laws+' limb-darkening coefficients'+'.png')

plt.show()
